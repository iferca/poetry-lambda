from invoke import run


def package():
    run("poetry build")
    run("poetry run pip install --upgrade -t dist/tmp_package dist/*.whl")
    run("cd dist/tmp_package ; zip -r ../artifact.zip . -x '*.pyc'")
    run("rm -rf dist/tmp_package")
