# Poetry :: AWS Lambda
This repo contains a very simple example on how to use poetry to handle python project and package the result as a zip 
ready to be used as an AWS Lambda function. 

## How to test

- Clone the repo
- Make sure poetry is [installed](https://python-poetry.org/docs/#installation) in your system
- Install deps and create environment: `poetry install`
- [Python's invoke package](https://www.pyinvoke.org/) will be installed, and `pyproject.toml` contains a script that uses
the `tasks.py` to do the build. Run `poetry run package` 
- The resulting deployable artifact will be in `dist/artifact.zip`. The zip file will contain the function code and all the dependencies

## Acknowledgment 

[Building Lambdas with Poetry by Keith Gregory](https://chariotsolutions.com/blog/post/building-lambdas-with-poetry/)